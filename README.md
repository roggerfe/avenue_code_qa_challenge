# avenue_code_qa_challenge

This project is a challenge proposed by Avenue Code for QA Automation Engineer role.
The purpose is to implement automated scenarios for a ToDo application that manages tasks and subtasks for a user

## Technologies/Libraries/Frameworks used

I developed the UI funcional tests in nodeJs javascript.
I used `webdriverio` and `cucumberjs` in BDD (Behavior Driven Development) to map the test scenarios.
The reason why I chose these libraries its because of its simplicity and agility

## Pre-requisites

To run the tests, it is necessary to have following softwares:

- Google Chrome Updated
- NodeJS and NPM (v8.0.0)

## Instructions to run tests

After checkout the project, in order to run the automated tests, simply run (the command below will run every features):

```sh
npm install && npm run clean && npm test
```

- In order to run a specific feature, simply edit the `specs` key in `wdio.conf.js` file

- In order to run with google chrome GUI interacting (non-headless), simply comment `chromeOptions` key in `wdio.conf.js`
