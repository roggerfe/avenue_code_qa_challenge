const { Given, When, Then } = require("cucumber");

const subtasksPage = require("./../../src/pages/subtasks.page");
const utils = require("./../../src/utils/utils");

Then(
  /^I expect to see task description "([^"]*)?" in manage subtask modal$/,
  function(name) {
    expect(
      browser
        .element(subtasksPage.selectors.taskDescription)
        .getAttribute("value")
    ).to.equal(name);
  }
);

Then(
  /^task description should be read only in manage subtask modal$/,
  function() {
    expect(browser.element(subtasksPage.selectors.taskDescription).isEnabled())
      .to.be.false;
  }
);

When(
  /^I add a subtask with description "([^"]*)?" and date "([^"]*)?"$/,
  function(description, date) {
    subtasksPage.createItem(description, date);
  }
);

Then(/^subtask items should be empty$/, function() {
  subtasksPage.assertEmpty();
});

Then(/^subtasks should have following detailed items:$/, function(items) {
  utils.assertJsonEqual(
    subtasksPage.jsonFromTable(items),
    utils.dataTableToJson(items)
  );
});

Then(/^I close subtasks modal$/, function() {
  browser.click(subtasksPage.selectors.closeButton);
});

Then(/^I mark subtask "([^"]*)?" as done$/, function(item) {
  var row = subtasksPage.getRow(item);
  row.element(subtasksPage.selectors.subtaskDoneTable).click();
});

Then(/^I edit subtask "([^"]*)?" to "([^"]*)?"$/, function(oldName, newName) {
  subtasksPage.editDescription(oldName, newName);
});
