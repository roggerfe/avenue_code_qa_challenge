const { Given, When } = require("cucumber");

const basePage = require("./../../src/pages/base.page");

Given(/^I open the application with url "([^"]*)?"$/, path => {
  basePage.open(path);
});

When(/^I click in "([^"]*)?" link in the navbar$/, menu => {
  basePage.menuClick(menu);
});
