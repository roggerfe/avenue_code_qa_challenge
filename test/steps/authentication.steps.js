const { Given, When, Then } = require("cucumber");

const loginPage = require("./../../src/pages/login.page");
const basePage = require("./../../src/pages/base.page");

var loginAction = function(email, password) {
  if (email && password) {
    browser.setValue(loginPage.selectors.email, email);
    browser.setValue(loginPage.selectors.password, password);
  }
  browser.click(loginPage.selectors.loginButton);
};

When(
  /^I login with email "([^"]*)?" and password "([^"]*)?"$/,
  (email, password) => {
    loginAction(email, password);
  }
);

Then(/^I expect to get invalid email or password error$/, () => {
  loginPage.assertLoginNotSuccessful();
});

Then(/^I expect to be corretly logged in as "([^"]*)?"$/, name => {
  loginPage.assertLoginSuccessful(name);
});

Given(/^I am logged in as "([^"]*)?" and password "([^"]*)?"$/, function(
  email,
  password
) {
  basePage.open("/users/sign_in");
  if (
    !browser
      .element(".navbar")
      .getText()
      .includes("Welcome")
  ) {
    loginAction(email, password);
  } else {
    console.log("User", email, "already logged in");
  }
});
