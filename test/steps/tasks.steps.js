const { Given, When, Then } = require("cucumber");

const tasksPage = require("./../../src/pages/tasks.page");

Then(/^I should see that this list belongs to "([^"]*)?"$/, name => {
  expect(
    browser
      .element(tasksPage.selectors.userInfo)
      .getText()
      .split("'")[0]
  ).to.equal(name);
});

Then(/^I expect the tasks list to be empty$/, () => {
  tasksPage.assertEmpty();
});

Given(/^I remove all remaining items from list$/, () => {
  tasksPage.removeAll();
});

Given(
  /^I attempt to insert a task called "([^"]*)?" with "([^"]*)?"$/,
  (taskName, approach) => {
    console.log("Inserting", taskName, "with", approach);
    tasksPage.createItem(taskName, approach === "Enter" ? true : false);
  }
);

Then(/^I expect the tasks list to contain "([^"]*)?"$/, function(taskName) {
  tasksPage.assertIfHas(taskName);
});

When(/^I edit the item "([^"]*)?" with new description "([^"]*)?"$/, function(
  taskName,
  newTaskName
) {
  tasksPage.edit(taskName, newTaskName);
});

Then(
  /^I get the message error "([^"]*)?" when confirming edition of task$/,
  function(errorMessage) {
    tasksPage.assertErrorOnEdit(errorMessage);
  }
);

Then(/^I expect that taks named "([^"]*)?" has "([^"]*)?" subtasks$/, function(
  taskName,
  subtasksCount
) {
  tasksPage.assertCountSubtasks(taskName, subtasksCount);
});

When(/^I manage subtask of "([^"]*)?"$/, function(taskName) {
  browser.pause(4000);
  tasksPage.openSubtask(taskName);
});
