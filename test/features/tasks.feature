Feature: Test the ability to manage tasks in ToDo app
    As a ToDo App user
    I should be able to create a task
    So I can manage my tasks

    Background:
        Given I am logged in as "rogger@ac.com" and password "avenuecode@123"

    Scenario Outline: Verify if cannot create task with amount of characters out of interval [3,250] (Using Enter and Button)
        Given I open the application with url "/tasks"
        And I remove all remaining items from list
        Then I expect the tasks list to be empty
        When I attempt to insert a task called "<item_name>" with "<approach>"
        Then I expect the tasks list to be empty

        Examples:
            | item_name                                                                                                                                                                                                                                                   | approach |
            | ab                                                                                                                                                                                                                                                          | Enter    |
            | 12                                                                                                                                                                                                                                                          | Button   |
            |                                                                                                                                                                                                                                                             | Enter    |
            |                                                                                                                                                                                                                                                             | Button   |
            | This is a reallyyyyyyyyy long text that has more than 250 characters, actually the length of this text is 251 characters, so I am using boundary approach to test this condition. This item cannot be added to my list. Blahhhhhhhhhh blahhhhhhhhhhhhhhhhhh | Enter    |
            | This is a reallyyyyyyyyy long text that has more than 250 characters, actually the length of this text is 251 characters, so I am using boundary approach to test this condition. This item cannot be added to my list. Blahhhhhhhhhh blahhhhhhhhhhhhhhhhhh | Button   |


    Scenario: Redirecting to my tasks list and assert if list is empty
        When I click in "My Tasks" link in the navbar
        Then I should see that this list belongs to "Rogger Fernandes"
        Given I remove all remaining items from list
        Then I expect the tasks list to be empty


    Scenario Outline: Verify if user can create task using Enter and button
        Given I open the application with url "/tasks"
        And I remove all remaining items from list
        When I attempt to insert a task called <item_name> with <approach>
        Then I expect the tasks list to contain <item_name>

        Examples:
            | item_name      | approach |
            | "To Do item 1" | "Enter"  |
            | "To Do item 2" | "Button" |


    Scenario Outline: Verify if user cannot edit task (in grid) resulting in amount of characters not in interval [3,250]
        Given I open the application with url "/tasks"
        And I remove all remaining items from list
        When I attempt to insert a task called <item_name> with "Enter"
        Then I expect the tasks list to contain <item_name>
        When I edit the item <item_name> with new description <item_new_name>
        Then I get the message error <error_edition_form> when confirming edition of task

        Examples:
            | item_name      | item_new_name                                                                                                                                                                                                                                                 | error_edition_form                          |
            | "TODO ITEM #1" | ""                                                                                                                                                                                                                                                            | "Task can't be blank!"                      |
            | "TODO ITEM #1" | "aa"                                                                                                                                                                                                                                                          | "Task can't have less than 3 characters!"   |
            | "TODO ITEM #1" | "This is a reallyyyyyyyyy long text that has more than 250 characters, actually the length of this text is 251 characters, so I am using boundary approach to test this condition. This item cannot be added to my list. Blahhhhhhhhhh blahhhhhhhhhhhhhhhhhh" | "Task can't have more than 250 characters!" |