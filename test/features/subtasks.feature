Feature: Test the ability to manage subtasks in ToDo app
    As a ToDo App user
    I should be able to create a subtask
    So I can break down my tasks in smaller pieces

    Background:
        Given I am logged in as "rogger@ac.com" and password "avenuecode@123"
        And I open the application with url "/tasks"

    Scenario: Create task, verify if has no subtasks yet, and check if field Description is readonly
        Given I remove all remaining items from list
        When I attempt to insert a task called "Item with subitems" with "Enter"
        Then I expect the tasks list to contain "Item with subitems"
        And I expect that taks named "Item with subitems" has "0" subtasks
        When I manage subtask of "Item with subitems"
        Then I expect to see task description "Item with subitems" in manage subtask modal
        And task description should be read only in manage subtask modal

    Scenario: Check if user cannot create subtask with description and date empty
        Given I remove all remaining items from list
        When I attempt to insert a task called "Item with subitems" with "Enter"
        Then I expect the tasks list to contain "Item with subitems"
        When I manage subtask of "Item with subitems"
        And I add a subtask with description "" and date ""
        Then subtask items should be empty


    Scenario Outline: Check if due date only allow format mm/dd/yyyy
        Given I remove all remaining items from list
        When I attempt to insert a task called <task> with "Enter"
        Then I expect the tasks list to contain <task>
        When I manage subtask of <task>
        And I add a subtask with description <subtask> and date <date>
        Then subtask items should be empty

        Examples:
            | task                 | subtask      | date         |
            | "Item with subitems" | "Subitem #1" | "30/07/2018" |
            | "Item with subitems" | "Subitem #1" | "2018/07/30" |

    Scenario: Create subtasks, edit and verify if state is as expected
        Given I remove all remaining items from list
        When I attempt to insert a task called "New item test" with "Enter"
        Then I expect the tasks list to contain "New item test"
        When I manage subtask of "New item test"
        And I add a subtask with description "SUBITEM #1" and date "08/20/2018"
        And I add a subtask with description "SUBITEM #2" and date "09/20/2018"
        Then subtasks should have following detailed items:
            | done  | description |
            | false | SUBITEM #1  |
            | false | SUBITEM #2  |
        And I mark subtask "SUBITEM #1" as done
        And subtasks should have following detailed items:
            | done  | description |
            | true  | SUBITEM #1  |
            | false | SUBITEM #2  |
        And I close subtasks modal
        And I expect that taks named "New item test" has "2" subtasks
        When I manage subtask of "New item test"
        Then subtasks should have following detailed items:
            | done  | description |
            | true  | SUBITEM #1  |
            | false | SUBITEM #2  |


    Scenario Outline: Check if cannot allow subtask with less than 3 characters and more than 250 characters
        Given I remove all remaining items from list
        When I attempt to insert a task called <item_name> with "Enter"
        Then I expect the tasks list to contain <item_name>
        When I manage subtask of <item_name>
        And I add a subtask with description <subitem_name> and date "08/20/2018"
        Then subtask items should be empty

        Examples:
            | item_name          | subitem_name                                                                                                                                                                                                                                                  |
            | "New todo List #1" | "ab"                                                                                                                                                                                                                                                          |
            | "New todo List #1" | "This is a reallyyyyyyyyy long text that has more than 250 characters, actually the length of this text is 251 characters, so I am using boundary approach to test this condition. This item cannot be added to my list. Blahhhhhhhhhh blahhhhhhhhhhhhhhhhhh" |


    Scenario: Edit description and assert if subtask gets changed
        Given I remove all remaining items from list
        When I attempt to insert a task called "Test New Item" with "Enter"
        Then I expect the tasks list to contain "Test New Item"
        When I manage subtask of "Test New Item"
        And I add a subtask with description "New Subitem" and date "08/20/2018"
        Then subtasks should have following detailed items:
            | done  | description |
            | false | New Subitem |
        And I edit subtask "New Subitem" to "New Subitem Edited"
        And subtasks should have following detailed items:
            | done  | description        |
            | false | New Subitem Edited |
        And I close subtasks modal
        And I expect that taks named "Test New Item" has "1" subtasks
        Given I open the application with url "/tasks"
        Then I expect that taks named "Test New Item" has "1" subtasks
        When I manage subtask of "Test New Item"
        Then subtasks should have following detailed items:
            | done  | description        |
            | false | New Subitem Edited |


