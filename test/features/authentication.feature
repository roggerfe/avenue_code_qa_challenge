Feature: Test the System authentication
    As a ToDo app user
    I want to be able to login to the system

    Background:
        Given I open the application with url "/users/sign_in"

    Scenario Outline: Attempt to login with wrong/blank credentials
        When  I login with email <email> and password <password>
        Then I expect to get invalid email or password error

        Examples:
            | email                   | password         |
            | "rogger@ac.com"         | "ac@123"         |
            | "rogger@avenuecode.com" | "avenuecode@123" |
            | ""                      | ""               |

    Scenario: Login with correct credentials
        When I login with email "rogger@ac.com" and password "avenuecode@123"
        Then I expect to be corretly logged in as "Rogger Fernandes"

