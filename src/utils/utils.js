const Utils = function() {
  this.dataTableToJson = function(datatable) {
    var json = [];
    var headers = datatable.rawTable[0];

    for (var i = 1; i < datatable.rawTable.length; i++) {
      var item = {};
      for (var j = 0; j < datatable.rawTable[i].length; j++) {
        item[headers[j]] = this.stringToBoolean(datatable.rawTable[i][j]);
      }
      json.push(item);
    }

    return json;
  };

  this.assertJsonEqual = function(jsonCurr, jsonExpected) {
    expect(jsonCurr.length).to.equal(jsonExpected.length);

    for (var i = 0; i < jsonCurr.length; i++) {
      console.log("curr", JSON.stringify(jsonCurr[i]));
      console.log("expected", JSON.stringify(jsonExpected[i]));

      expect(jsonCurr[i]).to.eql(jsonExpected[i]);
    }
  };

  this.stringToBoolean = function(str) {
    if (str.toLowerCase() === "true") return true;
    if (str.toLowerCase() === "false") return false;
    return str;
  };
};

module.exports = new Utils();
