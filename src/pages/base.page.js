const BasePage = function() {
  this.open = function(path) {
    browser.url(browser.options.baseUrl + path);
  };

  /**
   * Call this function when there are pending requests needed
   */
  this.waitForRequests = function() {
    browser.waitUntil(
      function() {
        var status = browser.selectorExecute("body", function(el) {
          return document.readyState;
        });
        return status === "complete";
      },
      60000,
      "Expected requests done after 1 minute"
    );
  };

  /**
   * Returns an array of all alerts found at page
   */
  this.getAlerts = function() {
    //browser.debug();
    let alerts = browser.elements(".alert");
    if (alerts.value.length === 0) {
      throw new Error("No alerts have been found at the page");
    }
    return alerts.value;
  };

  this.menuClick = function(menu) {
    let links = browser.elements(".navbar a"); // get all links from the navbar
    if (links == undefined || links.value.length === 0)
      throw new Error("No links found at menu");

    let found = false;
    for (var i = 0; i < links.value.length; i++) {
      let currLinkElement = links.value[i].element();
      let currLinkText = currLinkElement.getText().trim();
      if (currLinkText === menu) {
        currLinkElement.click();
        found = true;
        break;
      }
    }

    if (!found)
      throw new Error("Error, could not find " + menu + " menu in the navbar");
  };
};

module.exports = new BasePage();
