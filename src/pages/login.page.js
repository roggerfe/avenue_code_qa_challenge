const basePage = require("./base.page");

const LoginPage = function() {
  this.selectors = {
    loggedUser: "a[href='#']",
    email: "#user_email",
    password: "#user_password",
    loginButton: "input[type='submit']"
  };

  this.assertLoginSuccessful = function(name) {
    let alerts = basePage.getAlerts();
    expect(alerts).to.have.lengthOf(1);

    expect(alerts[0].element().getText()).to.equal("Signed in successfully.");

    expect(browser.element(this.selectors.loggedUser).getText()).to.equal(
      "Welcome, " + name + "!"
    );
  };

  this.assertLoginNotSuccessful = function() {
    let alerts = basePage.getAlerts();
    expect(alerts).to.have.lengthOf(1);
    expect(alerts[0].element().getText()).to.equal(
      "Invalid email or password."
    );
  };
};

module.exports = new LoginPage();
