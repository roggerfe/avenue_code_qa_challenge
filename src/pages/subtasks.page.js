const basePage = require("./base.page");

const SubtasksPage = function() {
  var self = this;
  this.selectors = {
    /**
     * Fields to create new subtask
     */
    taskDescription: "#edit_task",
    subtaskDescription: "#new_sub_task",
    subtaskDate: "#dueDate",
    addButton: "#add-subtask",
    closeButton: "[ng-click='close()']",
    editDescription: ".modal-content input.editable-input",
    confirmEditButton: ".modal-content button[type='submit']",

    /**
     * Selectors of subtasks grid
     */
    rowSubtask: "[ng-repeat='sub_task in task.sub_tasks']",
    tableSubtask: ".modal-content table tbody",
    subtaskDoneTable: "[ng-model='sub_task.completed']",
    subtaskDescriptionTable: "[editable-text='sub_task.body']",
    subtaskRemoveTable: "[ng-click='removeSubTask(sub_task)']"
  };

  this.createItem = function(description, date) {
    if (description)
      browser.setValue(this.selectors.subtaskDescription, description);

    if (!date || date === "") {
      browser.clearElement(this.selectors.subtaskDate);
    } else browser.setValue(this.selectors.subtaskDate, date);

    browser.click(this.selectors.addButton);
  };

  /**
   * Verifies if list of subtasks has no item
   */
  this.assertEmpty = function() {
    basePage.waitForRequests();
    var textoList = browser
      .element(this.selectors.tableSubtask)
      .getText()
      .trim();
    expect(textoList).to.equal("");
  };

  this.removeAll = function() {
    basePage.waitForRequests();
    let removeButtons = browser.elements(this.selectors.subtaskRemoveTable);

    removeButtons.value.forEach(function(removeButton) {
      removeButton.click();
    });

    this.assertEmpty();
  };

  this.assertIfHas = function(item) {
    expect(
      browser.element(this.selectors.subtaskRemoveTable).getText()
    ).to.include(item);
  };

  this.getRow = function(stName) {
    var subtasks = browser.elements(this.selectors.rowSubtask);
    expect(subtasks.value.length).to.be.at.least(1);

    var found = false;
    for (var i = 0; i < subtasks.value.length; i++) {
      var descriptionElement = subtasks.value[i].element(
        this.selectors.subtaskDescriptionTable
      );
      var descriptonText = descriptionElement.getText().trim();
      if (descriptonText === stName) {
        found = true;
        return subtasks.value[i];
      }
    }
    if (!found) throw new Error("Could not find subtask " + stName);
  };

  this.editDescription = function(oldName, newName) {
    console.log("oldname", oldName);
    console.log("new name", newName);
    var row = this.getRow(oldName);
    row.element(this.selectors.subtaskDescriptionTable).click();
    browser.setValue(this.selectors.editDescription, newName);
    browser.click(this.selectors.confirmEditButton);

    // wait to complete
    browser.waitUntil(function() {
      console.log("amount of btns", row.elements("button").value.length);
      return row.elements("button").value.length != 3;
    }, 7000);
  };

  /**
   * Returns a json given data shown of the subtasks
   */
  this.jsonFromTable = function() {
    var json = [];

    basePage.waitForRequests();
    var subtasks = browser.elements(this.selectors.rowSubtask);
    subtasks.value.forEach(function(subtask) {
      var item = {};
      item.done = subtask.element(self.selectors.subtaskDoneTable).isSelected();
      if (item.done)
        expect(subtask.element().getAttribute("class")).to.include("success"); // assert if text is blank
      item.description = subtask
        .element(self.selectors.subtaskDescriptionTable)
        .getText()
        .trim();
      json.push(item);
    });

    return json;
  };
};

module.exports = new SubtasksPage();
