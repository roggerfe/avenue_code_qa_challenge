const basePage = require("./base.page");

const TasksPage = function() {
  this.selectors = {
    userInfo: "h1",
    newTaskInput: "#new_task",
    insertTaskButton: "[ng-click='addTask()']",

    /**
     * Selectors of table of tasks
     */
    table: "table tbody",
    taskRow: "tr[ng-repeat='task in tasks']",
    doneRow: "[ng-model='task.completed']",
    descriptionRow: "[editable-text='task.body']",
    publicRow: "[ng-model='task.public']",
    subtaskRow: "[ng-click='editModal(task)']",
    removeRow: "[ng-click='removeTask(task)']",
    editDescriptionInput: "input.editable-input",
    confirmEditionButton: "button[type='submit']",
    errorMsg: ".editable-error"
  };

  /**
   * Append new item to the ToDo list
   * @param {String} taskName name of task to be added
   * @param {Boolean} enter True if task should be added using enter Key, false/null/undefined will click the plus button instead
   */
  this.createItem = function(taskName, enter) {
    if (taskName) browser.setValue(this.selectors.newTaskInput, taskName);
    else console.log("Inserting empty item");

    if (enter) {
      console.log("using enter");
      // create item pressing Enter key
      browser.keys("Enter");
    } else {
      console.log("using button");
      // create item with the '+' button
      browser.click(this.selectors.insertTaskButton);
    }
  };

  /**
   * Verifies if list has no item
   */
  this.assertEmpty = function() {
    basePage.waitForRequests();
    var textoList = browser
      .element("table tbody")
      .getText()
      .trim();
    expect(textoList).to.equal("");
  };

  this.removeAll = function() {
    basePage.waitForRequests();
    let removeButtons = browser.elements(this.selectors.removeRow);

    removeButtons.value.forEach(function(removeButton) {
      removeButton.click();
    });

    this.assertEmpty();
  };

  this.assertIfHas = function(item) {
    expect(browser.element(this.selectors.table).getText()).to.include(item);
  };

  this.getRow = function(taskName) {
    var tasks = browser.elements(this.selectors.taskRow);
    expect(tasks.value.length).to.be.at.least(1);

    var found = false;
    for (var i = 0; i < tasks.value.length; i++) {
      var descriptionElement = tasks.value[i].element(
        this.selectors.descriptionRow
      );
      var descriptonText = descriptionElement.getText().trim();
      if (descriptonText === taskName) {
        found = true;
        return tasks.value[i];
      }
    }
    if (!found) throw new Error("Could not find task " + taskName);
  };

  this.edit = function(taskName, newTaskName) {
    var row = this.getRow(taskName);

    row.element(this.selectors.descriptionRow).click();
    browser.clearElement(this.selectors.editDescriptionInput);
    if (newTaskName)
      browser.setValue(this.selectors.editDescriptionInput, newTaskName);
    browser.click(this.selectors.confirmEditionButton);
  };

  this.assertErrorOnEdit = function(errorMsg) {
    expect(browser.element(this.selectors.errorMsg).getText()).to.equal(
      errorMsg
    );
  };

  this.assertCountSubtasks = function(taskName, taskCount) {
    var row = this.getRow(taskName);
    var taskCountCurrent = row
      .element(this.selectors.subtaskRow)
      .getText()
      .split("(")[1]
      .split(")")[0];

    expect(taskCountCurrent).to.equal(taskCount);
  };

  this.openSubtask = function(task) {
    var row = this.getRow(task);
    row.element(this.selectors.subtaskRow).click();
  };
};

module.exports = new TasksPage();
